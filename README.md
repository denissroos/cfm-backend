Konverentside halduse rakendus spetsifikatsioon
===========================================
Back-end rakendus - JAVA

Andmebaas - PostgreSQL

Tegemist on rakendusega, millega saab hallata konverentse. Funktsionaalsus (baasfunktsionaalsus, vajalikud lisategevused ):

* kasutaja loob konverentsi;
* kasutaja tühistab konverentsi;
* kasutaja kontrollib konverentsiruumide saadavust (vastavalt registreerunud kasutajatele)
* kasutaja lisab osalise konverentsi;
* kasutaja eemaldab osalise konverentsist.

Andmemudel (baasandmed):

* Konverents: konverentsi nimi, toimumise kuupäev ning algusaeg.
* Osaleja: nimi ja sünnipäev.
* Konverentsiruum: nimi, asukoht, mahutavus.

"Back-end"  paigaldusjuhend
===========================================
## versioon	0.0.1-SNAPSHOT	 
 
## Komponendid

    JDK 1.8.0.x x64
    Apache Tomcat/8.0.39

## Andmebaas

Rakendus kasutab oma tööks postgreSQL andmebaasi

"public" all asuvad tabelid:
conference
conference_room
participant

Initial andmed (tabelid, trigerid, indeksid jne) loob rakendus ise

## Rakenduse konfiguratsioon

application.properties
```
spring.datasource.driverClassName=org.postgresql.Driver
spring.datasource.url=jdbc:postgresql://localhost:5433/cfm-db
spring.datasource.username=cfmapp
spring.datasource.password=cfmapp

spring.jpa.database-platform=org.hibernate.dialect.PostgreSQLDialect
spring.jpa.generate-ddl=true
spring.jpa.show-sql=true
spring.jpa.hibernate.ddl-auto=create-drop
security.basic.enabled=false
management.security.enabled=false
server.contextPath=/cfm-backend
server.port = 8080
```
## Ehitamine ja paigaldamine

Ehita kokku war (Selleks kasuta nt. oma IDE)
Tee deploy Tomcat'ile kas käsitsi või kasutades manager tarkvara