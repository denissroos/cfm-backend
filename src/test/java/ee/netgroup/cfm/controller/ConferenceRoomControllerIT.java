package ee.netgroup.cfm.controller;

import ee.netgroup.cfm.IntegrationTestHelper;
import ee.netgroup.cfm.model.ConferenceRoom;
import ee.netgroup.cfm.repository.ConferenceRoomRepository;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.equalTo;
/**
 * Created by Deniss Roos on 5.12.2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class ConferenceRoomControllerIT extends IntegrationTestHelper {

    private static final String RESOURCE_PATH = "/conferenceroom/v1";

    RestTemplate restTemplate = new TestRestTemplate();
    private ConferenceRoom newConferenceRoom;

    @Before
    public void before(){

        this.newConferenceRoom= new ConferenceRoom();
        newConferenceRoom.setName("test");
        newConferenceRoom.setDescription("test");
        newConferenceRoom.setLocation("test");
        newConferenceRoom.setCapacity(10);

    }
    @Test
    public void saveNewConferenceRoom() throws Exception{
        //set headers
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        //set entity to send
        HttpEntity entity = new HttpEntity(newConferenceRoom,headers);
        //post entity
        ResponseEntity<ConferenceRoom> responseEntity = restTemplate.exchange( getEndpointUrl(RESOURCE_PATH), HttpMethod.POST,entity,ConferenceRoom.class);
        ConferenceRoom response = responseEntity.getBody();
        HttpStatus status = responseEntity.getStatusCode();
        //validate
        assertThat(status,equalTo(HttpStatus.OK));

    }
    @Test
    public void getConferenceRoomById() throws Exception{
        //get entity
        ResponseEntity<ConferenceRoom> responseEntity = restTemplate.exchange( getEndpointUrl(RESOURCE_PATH)+"/1", HttpMethod.GET,null,ConferenceRoom.class);
        HttpStatus status = responseEntity.getStatusCode();
        //validate
        assertThat(status,equalTo(HttpStatus.OK));

    }

    @Test
    public void getAll() throws Exception{
        //get entity
        ResponseEntity<List<ConferenceRoom>> responseEntity = restTemplate.exchange( getEndpointUrl(RESOURCE_PATH), HttpMethod.GET,null,new ParameterizedTypeReference<List<ConferenceRoom>>(){});
        //List<ConferenceRoom> response = responseEntity.getBody();
        HttpStatus status = responseEntity.getStatusCode();
        //validate
        assertThat(status,equalTo(HttpStatus.OK));
    }


}
