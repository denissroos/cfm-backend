package ee.netgroup.cfm;


/**
 * Created by Deniss Roos on 7.12.2016.
 */

public class IntegrationTestHelper {

    private static final String serverUrl = "http://localhost:8080/cfm-backend";

    public String getEndpointUrl(String resourcePath){
        return serverUrl + resourcePath;
    }

}
