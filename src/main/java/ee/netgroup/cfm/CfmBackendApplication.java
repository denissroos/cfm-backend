package ee.netgroup.cfm;

import ee.netgroup.cfm.config.AppConfig;
import ee.netgroup.cfm.service.initial.DataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.retry.annotation.EnableRetry;

import javax.annotation.PostConstruct;

@Import({AppConfig.class})
@EnableRetry
@SpringBootApplication
@Configuration
@EnableAutoConfiguration
@PropertySource("classpath:internal.properties")
@PropertySource(value = "file:/app/cfm-backend/conf/cfm-backend.properties", ignoreResourceNotFound = true)
public class CfmBackendApplication extends SpringBootServletInitializer {


	@Autowired
	DataService dataService;

	public static void main(String[] args) throws Exception {
		SpringApplication.run(CfmBackendApplication.class, args);
	}

	@PostConstruct
	public void addDataForDemonstration(){
		dataService.initial();
	}



}

