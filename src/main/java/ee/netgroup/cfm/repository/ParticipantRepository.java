package ee.netgroup.cfm.repository;

import ee.netgroup.cfm.model.Conference;
import ee.netgroup.cfm.model.Participant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


/**
 * Created by Deniss Roos on 3.12.2016.
 */
public interface ParticipantRepository extends JpaRepository<Participant,Integer> {

    @Query( value = "select count(*) from Participant where conference =?1")
    Integer countByConference(Conference conference);
}
