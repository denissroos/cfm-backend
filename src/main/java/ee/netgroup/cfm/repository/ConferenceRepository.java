package ee.netgroup.cfm.repository;

import ee.netgroup.cfm.model.Conference;
import ee.netgroup.cfm.model.ConferenceRoom;
import org.springframework.data.jpa.repository.JpaRepository;


/**
 * Created by Deniss Roos on 3.12.2016.
 */
public interface ConferenceRepository extends JpaRepository<Conference,Integer> {

}
