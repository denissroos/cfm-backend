package ee.netgroup.cfm.service;

import ee.netgroup.cfm.enums.ExceptionEnum;
import ee.netgroup.cfm.exceptions.CfmException;
import ee.netgroup.cfm.model.Conference;
import ee.netgroup.cfm.model.ConferenceRoom;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ee.netgroup.cfm.model.Participant;
import ee.netgroup.cfm.repository.ParticipantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Deniss Roos on 4.12.2016.
 */
@Service
public class ParticipantService {

    final static Logger logger = LoggerFactory.getLogger(ParticipantService.class);

    @Autowired
    private ParticipantRepository participantRepo;


    public List<Participant> findAll() {
        return participantRepo.findAll();
    }

    public Participant show(Integer id) {
        return participantRepo.findOne(id);
    }

    public Participant save(Participant item) throws CfmException {

        checkConferenceRoomByConference(item.getConference());
        return participantRepo.saveAndFlush(item);
    }

    public void delete(Integer id) {
        participantRepo.delete(id);
    }
    public void checkConferenceRoomByConference(Conference conference)throws CfmException{
        ConferenceRoom conferenceRoom = conference.getConferenceRoom();
        int capacity = conferenceRoom.getCapacity();
        if(participantRepo.countByConference(conference)==   capacity) {
            throw new CfmException(ExceptionEnum.WARNING_THE_NUMBER_OF_PARTICIPANTS_EXCEEDS);
        }
    }
}
