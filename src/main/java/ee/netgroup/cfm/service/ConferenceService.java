package ee.netgroup.cfm.service;

import ee.netgroup.cfm.model.Conference;
import ee.netgroup.cfm.repository.ConferenceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Deniss Roos on 2.12.2016.
 */

@Service
public class ConferenceService {

    @Autowired
    private ConferenceRepository conferenceRepo;

    @Autowired
    private ParticipantService participantService;
    public List<Conference> findAll() {

        return conferenceRepo.findAll();
    }
    public Conference show(int id){
        return conferenceRepo.findOne(id);
    }
    public Conference save(Conference item) {

        return conferenceRepo.saveAndFlush(item);
    }

    public void delete(Integer id) {

        conferenceRepo.delete(id);
    }

}
