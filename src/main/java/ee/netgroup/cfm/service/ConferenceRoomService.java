package ee.netgroup.cfm.service;

import ee.netgroup.cfm.model.ConferenceRoom;
import ee.netgroup.cfm.repository.ConferenceRepository;
import ee.netgroup.cfm.repository.ConferenceRoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Deniss Roos on 4.12.2016.
 */
@Service
public class ConferenceRoomService {
    @Autowired
    private ConferenceRoomRepository conferenceRoomRepo;
    @Autowired
    private ConferenceRepository conferenceRepo;
    public List<ConferenceRoom> findAll() {
        return conferenceRoomRepo.findAll();
    }

    public ConferenceRoom save(ConferenceRoom item) {
        return conferenceRoomRepo.saveAndFlush(item);
    }

    public ConferenceRoom show(Integer id) {
        return conferenceRoomRepo.findOne(id);
    }

    public void delete(Integer id) {

        conferenceRoomRepo.delete(id);

    }
}
