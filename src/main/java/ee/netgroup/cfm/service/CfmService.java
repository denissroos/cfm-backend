package ee.netgroup.cfm.service;

import ee.netgroup.cfm.repository.ConferenceRepository;
import ee.netgroup.cfm.repository.ConferenceRoomRepository;
import ee.netgroup.cfm.repository.ParticipantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Deniss Roos on 2.12.2016.
 */
@Service
public class CfmService {

    @Autowired
    ConferenceRoomRepository conferenceRoomRepo;
    @Autowired
    ConferenceRepository conferenceRepo;
    @Autowired
    ParticipantRepository participantRepo;
    public Map countAll() {
        Map total= new HashMap();
        total.put("rooms",conferenceRoomRepo.count());
        total.put("conferences",conferenceRepo.count());
        total.put("participants",participantRepo.count());
        return total;
    }
}

