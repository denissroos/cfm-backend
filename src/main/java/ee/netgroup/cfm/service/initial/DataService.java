package ee.netgroup.cfm.service.initial;

import ee.netgroup.cfm.model.Conference;
import ee.netgroup.cfm.model.ConferenceRoom;
import ee.netgroup.cfm.model.Participant;
import ee.netgroup.cfm.repository.ConferenceRepository;
import ee.netgroup.cfm.repository.ConferenceRoomRepository;
import ee.netgroup.cfm.repository.ParticipantRepository;
import ee.netgroup.cfm.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Deniss Roos on 3.12.2016.
 */
@Component
@Service
public class DataService {

    @Autowired
    private DateUtils dateUtils;
    @Autowired
    private ConferenceRoomRepository conferenceRoomRepo;
    @Autowired
    private ConferenceRepository conferenceRepo;
    @Autowired
    private ParticipantRepository participantRepo;

    /**
     * Adding some data for demonstration
     */
    public void initial(){

        //  create new room for conference
        ConferenceRoom newConferenceRoom = new ConferenceRoom();

        newConferenceRoom.setCapacity(3);
        newConferenceRoom.setName("SCRUM");
        newConferenceRoom.setDescription("Ruum koosolekute läbiviimiseks");
        newConferenceRoom.setLocation("Netgroup OÜ, Lelle 22, Tallinn");
        conferenceRoomRepo.saveAndFlush(newConferenceRoom);

        //  create new conference
        Conference newConference= new Conference();
        newConference.setName("Töövestlus- II voor");
        newConference.setDescription("Ülesande valmislahenduse tutvustamine");
        newConference.setStartTime(dateUtils.setDateTime("2016-12-08 14:00"));
        newConference.setConferenceRoom(newConferenceRoom);
        conferenceRepo.saveAndFlush(newConference);

        // add participants into conference
        List<Participant> participants = new ArrayList<>();
        participants.add(createParticipant("Evelin","Luik","evelin.luik@netgroup.ee",newConference));
        participants.add(createParticipant("Sven","Peekmann","sven.peekmann@netgroup.ee",newConference));
        participants.add(createParticipant("Margus","Jõever","margus.joever@netgroup.ee.",newConference));

        for(Participant participant :participants){
            participantRepo.saveAndFlush(participant);
        }

    }
    private Participant createParticipant(String firstname,String lastname, String email, Conference conference){
        Participant participant= new Participant();
        participant.setFirstname(firstname);
        participant.setLastname(lastname);
        participant.setEmail(email);
        participant.setConference(conference);
        return participant;
    }

}
