package ee.netgroup.cfm.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * Created by Deniss Roos on 2.12.2016.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Participant implements Serializable {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;
    private Conference conference;
    private String firstname;
    private String lastname;
    private String email;
    @Column(nullable = true)
    @Temporal(TemporalType.DATE)
    private Date birthdate;
    private Date dateCreated;
    private Date lastUpdated;
    @PrePersist
    protected void onCreate() {
        dateCreated = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
        lastUpdated = new Date();
    }
}
