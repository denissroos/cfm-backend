package ee.netgroup.cfm.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by Deniss Roos on 2.12.2016.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class ConferenceRoom implements Serializable {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String description;
    private String location;
    private Integer capacity;
    private Date dateCreated;
    private Date lastUpdated;
    @PrePersist
    protected void onCreate() {
        dateCreated = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
        lastUpdated = new Date();
    }

}
