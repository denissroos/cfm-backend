package ee.netgroup.cfm.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ManyToAny;
import org.hibernate.sql.Delete;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * Created by Deniss Roos on 2.12.2016.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity

public class Conference implements Serializable {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;
    @ManyToOne
    private ConferenceRoom conferenceRoom;
    private String name;
    private String description;
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm")
    private Date startTime;
    private Date dateCreated;
    private Date lastUpdated;

    @PrePersist
    protected void onCreate() {
        dateCreated = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
        lastUpdated = new Date();
    }

}
