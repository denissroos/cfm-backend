package ee.netgroup.cfm.controller;


import ee.netgroup.cfm.exceptions.CfmException;
import ee.netgroup.cfm.model.Participant;
import ee.netgroup.cfm.service.ParticipantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Deniss Roos on 5.12.2016.
 */
@CrossOrigin
@RestController
@RequestMapping("/participant/v1")
public class ParticipantController extends BaseController{
    @Autowired
    ParticipantService participantService;

    @RequestMapping(method = RequestMethod.GET)
    public List<Participant> findItemd(){
        return participantService.findAll();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public @ResponseBody
    Participant getItem(@PathVariable Integer id, HttpServletRequest request) {
        logRequest(request);
        return participantService.show(id);

    }

    @RequestMapping(method = RequestMethod.POST)
    public Participant addItem(@RequestBody Participant item) throws CfmException{

        return participantService.save(item);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public Participant updateItem(@RequestBody Participant updatedItem) throws CfmException {
        return participantService.save(updatedItem);

    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteItem(@PathVariable Integer id) {
        participantService.delete(id);

    }
}
