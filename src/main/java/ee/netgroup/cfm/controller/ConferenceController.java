package ee.netgroup.cfm.controller;

import ee.netgroup.cfm.model.Conference;
import ee.netgroup.cfm.service.ConferenceService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Deniss Roos on 2.12.2016.
 */
@CrossOrigin
@RestController
@Retryable(maxAttempts = 5,value=RuntimeException.class,backoff = @Backoff(delay=1000,multiplier = 1))
@RequestMapping("/conference/v1")
public class ConferenceController extends BaseController {

    @Autowired
    ConferenceService conferenceService;
    @RequestMapping(method = RequestMethod.GET)
    public List<Conference> findItems(){

        return conferenceService.findAll();
    }
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public @ResponseBody Conference getItem(@PathVariable Integer id,HttpServletRequest request) {
        logRequest(request);
        return conferenceService.show(id);

    }
    @RequestMapping(method = RequestMethod.POST)
    public Conference addItem(@RequestBody Conference item) {

        return conferenceService.save(item);
    }
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public Conference updateItem(@RequestBody Conference updatedItem) {
        return conferenceService.save(updatedItem);
    }
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteItem(@PathVariable Integer id) {
        conferenceService.delete(id);

    }
}
