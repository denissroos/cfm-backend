package ee.netgroup.cfm.controller;

import ee.netgroup.cfm.model.ConferenceRoom;
import ee.netgroup.cfm.service.ConferenceRoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Deniss Roos on 4.12.2016.
 */
@CrossOrigin
@RestController
@RequestMapping("/conferenceroom/v1")
public class ConferenceRoomController extends BaseController {
    @Autowired
    ConferenceRoomService conferenceRoomService;

    @RequestMapping(method = RequestMethod.GET)
    public List<ConferenceRoom> findItemd(){
        return conferenceRoomService.findAll();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public @ResponseBody ConferenceRoom getItem(@PathVariable Integer id,HttpServletRequest request) {
        logRequest(request);
        return conferenceRoomService.show(id);

    }

    @RequestMapping(method = RequestMethod.POST)
    public ConferenceRoom addItem(@RequestBody ConferenceRoom item){

        return conferenceRoomService.save(item);
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.PUT)
    public ConferenceRoom updateItem(@RequestBody ConferenceRoom updatedItem) {
        return conferenceRoomService.save(updatedItem);

    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteItem(@PathVariable Integer id) {
        conferenceRoomService.delete(id);

    }
}
