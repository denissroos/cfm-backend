package ee.netgroup.cfm.controller;

import ee.netgroup.cfm.service.CfmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * RestApi endpoint for show statistic
 * Created by Deniss Roos on 2.12.2016.
 */
@RestController
@CrossOrigin
public class DashboardController extends BaseController{

    @Autowired
    private CfmService cfmService;
    @RequestMapping("/dashboard/v1")
    @ResponseBody
    public Map overview(){
        return cfmService.countAll();
    }

}
