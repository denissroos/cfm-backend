package ee.netgroup.cfm.controller;

import ee.netgroup.cfm.config.AppConfig;
import ee.netgroup.cfm.exceptions.CfmException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


/**
 * Created by Deniss Roos on 2.12.2016.
 */

/**
 * Include loggers, monitoring, exceptions handling functionality
 */
public class BaseController {

    final static Logger logger = LoggerFactory.getLogger(BaseController.class);
    private  static final List<String> methodsWithBodyObject = Arrays.asList(RequestMethod.POST.toString(),RequestMethod.PUT.toString());
    @Autowired
    public AppConfig ctrlConfig;

    public void logRequest( HttpServletRequest request){

        String ipAddress = ( request.getHeader("X-Forwarded-For") != null )?request.getHeader("X-Forwarded-For"):request.getRemoteAddr();

        logger.info("IP:"+ ipAddress);
        logger.info("Remote user:"+request.getRemoteUser());
        logger.info("URI:"+ request.getRequestURI());

        if(methodsWithBodyObject.contains(request.getMethod())){
            // for POST and PUT methods log request payload as MAP
            logger.info("Request body:"+ request.getParameterMap());
        }
    }
    public void logResponse(HttpServletResponse response){
        //TODO: make response object logging with skipping sensitive data
    }

    @ExceptionHandler(Exception.class)
    public void handleException(Throwable ex){
        //Something wrong with app
        logger.error(ex.getMessage());
    }

    @ExceptionHandler(CfmException.class)
    @ResponseBody
    @ResponseStatus(value = HttpStatus.NOT_ACCEPTABLE)
    public HashMap<String,CfmException> handleCfmException(CfmException exc){
        //hande custom App exceptions
        logger.warn(exc.getErrorCode());
        HashMap  responseMsg = new HashMap();
        responseMsg.put("errorCode", exc.getErrorCode());
        return responseMsg;
    }

}
