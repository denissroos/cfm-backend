package ee.netgroup.cfm.utils;

import org.springframework.stereotype.Component;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Deniss Roos on 3.12.2016.
 */
@Component
public class DateUtils {

    public static final String TIMESTAMP_FORMAT = "yyyy-MM-dd HH:mm";
    public Date setDateTime(String dateTime){
        Date newDate = null;
        try {
            SimpleDateFormat formatter = new SimpleDateFormat(TIMESTAMP_FORMAT);
            newDate=  formatter.parse(dateTime);
        } catch (ParseException e) {
            System.out.println(e.toString());
            e.printStackTrace();
        }
        return newDate;
    }
}
