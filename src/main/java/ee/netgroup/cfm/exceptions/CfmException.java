package ee.netgroup.cfm.exceptions;

import ee.netgroup.cfm.enums.ExceptionEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

/**
 * Created by Deniss Roos on 5.12.2016.
 */

public class CfmException extends Exception{

    private ExceptionEnum exc;

    public  CfmException(ExceptionEnum exc){
        this.exc = exc;
    }
    public String getErrorCode(){
        return exc.getErrorCode();
    }
    public HttpStatus getStatus(){
        return exc.getStatus();
    }
}
