package ee.netgroup.cfm.enums;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

/**
 * Created by Deniss Roos on 5.12.2016.
 */

public enum ExceptionEnum {

    WARNING_THE_NUMBER_OF_PARTICIPANTS_EXCEEDS("warning.the.number.of.participants.exceeds", HttpStatus.METHOD_NOT_ALLOWED);

    private final String errorCode;
    private final HttpStatus status;

    ExceptionEnum(String errorCode, HttpStatus status) {
        this.errorCode= errorCode;
        this.status = status;
    }
    public String getErrorCode(){
        return errorCode;
    }
    public HttpStatus getStatus(){
        return status;
    }
}
